package com.galvanize;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Rental extends Item {

    private BigDecimal rentalPricePerDay;
    private LocalDateTime endDate;


    public Rental(BigDecimal rentalPricePerDay, LocalDateTime endDate) {
        this.rentalPricePerDay = rentalPricePerDay;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Rental{" +
                "rentalPricePerDay=" + rentalPricePerDay +
                ", endDate=" + endDate +
                '}';
    }

    @Override
    public BigDecimal totalPrice() {
        long days = LocalDateTime.now().until(getEndDate(), ChronoUnit.DAYS) + 1;
        return getRentalPricePerDay().multiply(BigDecimal.valueOf(days));
    }



    public BigDecimal getRentalPricePerDay() {
        return this.rentalPricePerDay;
    }

    public LocalDateTime getEndDate() {
        return this.endDate;
    }
}
