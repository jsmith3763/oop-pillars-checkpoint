package com.galvanize;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Lease extends Item{

    private BigDecimal pricePerMonth;
    private int numberOfMonths;
    private String licensePlate;


    public Lease(String licensePlate, BigDecimal pricePerMonth, int numberOfMonths) {
        this.licensePlate = licensePlate;
        this.pricePerMonth = pricePerMonth;
        this.numberOfMonths = numberOfMonths;
    }

    @Override
    public String toString() {
        return "Lease{" +
                "pricePerMonth=" + pricePerMonth +
                ", numberOfMonths=" + numberOfMonths +
                ", licensePlate='" + licensePlate + '\'' +
                '}';
    }

    @Override
    public BigDecimal totalPrice() {
        return this.getPricePerMonth().multiply(BigDecimal.valueOf(this.getNumberOfMonths()));
    }



    public BigDecimal getPricePerMonth() {
        return pricePerMonth;
    }

    public int getNumberOfMonths() {
        return numberOfMonths;
    }

    public String getLicensePlate() { return licensePlate;}
}
