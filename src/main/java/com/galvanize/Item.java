package com.galvanize;

import java.math.BigDecimal;

public abstract class Item {

    private BigDecimal actualPrice = new BigDecimal("0.00");

    public BigDecimal totalPrice() {
        return actualPrice;
    }

}
