package com.galvanize;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Order {

    private ArrayList<Item> items = new ArrayList<>();
    private BigDecimal total = new BigDecimal("0.00");

    public void addItem(Item item) {
        items.add(item);
        total = total.add(item.totalPrice());
    }

    public BigDecimal getTotal() {
        return total;
    }

    public ArrayList<Item> getItems() {
        return items;
    }
}
