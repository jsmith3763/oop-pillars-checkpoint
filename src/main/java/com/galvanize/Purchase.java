package com.galvanize;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Purchase extends Item{

    private BigDecimal price;
    private String productName;


    public Purchase(String productName, BigDecimal price) {
        this.productName = productName;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "price=" + price +
                ", productName='" + productName + '\'' +
                '}';
    }

    @Override
    public BigDecimal totalPrice() {
        return getPrice();
    }

    public BigDecimal getPrice() {
        return this.price;
    }
}
